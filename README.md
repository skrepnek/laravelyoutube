IMPORTANT:
DO NOT FORGET TO SET THE GOOGLE_API_KEY IN .env FILE

Project laravelyoutube

Objective:
Create an application that can list youtube videos based on keywords (search)

Preliminar Analysis:
No need for a database.
Will use a separate internal API Layer to retrieve Google API data.

Initial Plan:
Use of MVVM (to be decided)
Simple interface: search bar on top with button to search
Results in grid format. Mobile will be vertical list

Version 0.0.1
Frameworks / Libraries installed:

- Laravel
- Vue
- Vue-Router
- Google Client
