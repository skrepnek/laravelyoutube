<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Video;
use Illuminate\Http\Request;

class VideosController extends Controller
{
    public function search($searchTerm)
    {
        return response()->json(Video::search($searchTerm));
    }
}
