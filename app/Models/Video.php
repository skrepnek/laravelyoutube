<?php

namespace App\Models;

use Google_Client;
use Google_Service_YouTube;

class Video
{
    public static function search($searchTerm)
    {
        $client = new Google_Client();
        $client->setApplicationName('Laravel Youtube');
        $client->setScopes([
            'https://www.googleapis.com/auth/youtube.readonly',
        ]);
        $client->setDeveloperKey(env('GOOGLE_API_KEY', false));

        $service = new Google_Service_YouTube($client);

        $queryParams = [
            'maxResults' => 25,
            'q' => $searchTerm,
            'type' => 'video'
        ];

        $response = $service->search->listSearch('snippet', $queryParams);

        return $response;
    }
}
