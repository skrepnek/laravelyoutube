import VueRouter from "vue-router";

import VideosIndex from "./videos/VideosIndex.vue";
import Videos from "./videos/Videos.vue";

const routes = [
    {
        path: "/",
        component: VideosIndex,
        name: "home",
    },
    {
        path: "/search/:searchTerm",
        component: Videos,
        name: "search",
        props: true,
    },
];

const router = new VueRouter({
    routes,
    mode: "history",
});

export default router;
